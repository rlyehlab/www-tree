import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import { join } from 'node:path'
import { font } from '@peterek/vite-plugin-font'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    svelte(),
    font({
      font: {
        name: 'Parametric',
        src: join(__dirname, 'assets/Parametric*.ttf'),
      },
    })
  ],
})
